package it.eng.rspa;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;


public class MyUtils {
	
	
	public static final String KEY_PILOT = "LIFERAY_SHARED_pilot";
	public static final String CUSTOMFIELD_PILOT = "pilot";
	public final static String PILOT_CITY_BILBAO = "Bilbao";
	public final static String PILOT_CITY_UUSIMAA = "Uusimaa";
	public final static String PILOT_CITY_NOVISAD = "Novisad";
	public final static String PILOT_CITY_TRENTO = "Trento";
	public final static String PILOT_ODS_BILBAO = "bilbao-city-council";
	public final static String PILOT_ODS_UUSIMAA = "helsinki-uusimaa";
	public final static String PILOT_ODS_NOVISAD = "novi-sad";
	public final static String PILOT_ODS_TRENTO = "trento";
	
	/**
	 * @param renderRequest
	 * @return
	 */
	public static String getPilot (RenderRequest request){
		
		String pilot = "";
		
		User currentUser = null;
		
			 try {
				currentUser = PortalUtil.getUser(request);
			} catch (PortalException e1) {
				e1.printStackTrace();
			} catch (SystemException e1) {
				e1.printStackTrace();
			}
			
		
		try {
		
			if (Validator.isNull(currentUser)){
			
				PortletSession sessionUser = request.getPortletSession();
				pilot = (String) sessionUser.getAttribute(KEY_PILOT, PortletSession.APPLICATION_SCOPE); //this value is set by another tool: the Controller
			}else{
				
				
				String[] pilotUtente = (String[]) currentUser.getExpandoBridge().getAttribute(CUSTOMFIELD_PILOT);
				pilot = pilotUtente[0];
				
			}
		
		
		}	 catch (Exception e) {
			
			e.printStackTrace();
			return pilot;
		}
		
		if (pilot == null)
			return "";
		
		
		return pilot;
		
	}

	/**
	 * @param acronimo
	 * @return
	 */
	public static String getODSPilotNameByPilot (String pilot){
		
		
		Map<String, String> pilots = new HashMap<String, String>();
		pilots.put(PILOT_CITY_BILBAO, PILOT_ODS_BILBAO);
		pilots.put(PILOT_CITY_NOVISAD,PILOT_ODS_NOVISAD );
		pilots.put(PILOT_CITY_UUSIMAA, PILOT_ODS_UUSIMAA);
		pilots.put(PILOT_CITY_TRENTO, PILOT_ODS_TRENTO);
		
		String val = pilots.get(pilot);		
		
		if (Validator.isNull(val))
			val = PILOT_ODS_BILBAO;
		
		return val;
	}
	
	
}
