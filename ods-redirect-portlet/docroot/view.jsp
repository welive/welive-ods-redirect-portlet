
<%@page import="javax.portlet.RenderRequest"%>
<%@page import="it.eng.rspa.MyUtils"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<%!
RenderRequest renderRequest;
%>

<portlet:defineObjects />

<p>
You are being redirected.......
</p>
<% 

ThemeDisplay themeDisplay = (ThemeDisplay)request.getAttribute(WebKeys.THEME_DISPLAY);
String lang = themeDisplay.getLanguageId();
String pilot = MyUtils.getPilot(renderRequest);
String odsPilot = MyUtils.getODSPilotNameByPilot(pilot);
String url = "/ods/";
	
	if (lang.equals("it_IT"))
		url = "/ods/it/organization/"+odsPilot;
	else if (lang.equalsIgnoreCase("fi_FI")) 
		url = "/ods/fi/organization/"+odsPilot;
	else if (lang.equalsIgnoreCase("sr_RS") || lang.equalsIgnoreCase("sr_RS_latin")) 
		url = "/ods/sr_Latn/organization/"+odsPilot;
	else if (lang.equalsIgnoreCase("es_ES")) 
		url = "/ods/es/organization/"+odsPilot;
	else 
		url = "/ods/en/organization/"+odsPilot;
	
	
	if (themeDisplay.isSignedIn()){
		url+="?login=true";
	}
%>
		<script>location.href="<%=url%>";</script>
<%return;%>